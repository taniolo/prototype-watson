﻿using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class SoundManager : MonoBehaviour
{
    public AudioClip[] Sounds;
    private AudioSource _audioSource;

    public void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    public void PlaySound()
    {
        if (_audioSource.isPlaying)
        {
            _audioSource.Stop();
        }
        var index = (int)Random.Range(0f, Sounds.Length);
        _audioSource.clip= Sounds[index];
       
        _audioSource.Play();
    }
}
