﻿using UnityEngine;

public class ActiveOnListening : MonoBehaviour
{
    public bool IsOn;
    private SpriteRenderer _renderer;

    private void Awake()
    {
        _renderer = GetComponent<SpriteRenderer>();
        GameController.OnStartListening.AddListener(() => SetIcon(IsOn));
        GameController.OnStopListening.AddListener(() => SetIcon(!IsOn));
    }

    private void SetIcon(bool active)
    {
        _renderer.enabled = active;
    }
}