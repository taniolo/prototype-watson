﻿using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;

public class Item : MonoBehaviour
{
    public string[] MatchWords;
    public GameObject[] Parts;
    public Animator Rig;
    public GameObject Silhouette;
    private int _currentPart;
    private List<Vector3> _partsTargets = new List<Vector3>();

    public UnityEvent OnRecognize = new UnityEvent();

    private void Awake()
    {
        foreach (var p in Parts)
        {
            _partsTargets.Add(p.transform.position);
            p.SetActive(false);
        }

        gameObject.SetActive(false);
    }

	private void Start()
	{
	    GameController.OnMatch.AddListener(Recognized);
	}

    private void Recognized()
    {
        Debug.Log("Current Item Recognized Successfully");
        Parts[_currentPart].transform.DOMove(_partsTargets[_currentPart], 2f).OnComplete(Snap);
    }

    private void Snap()
    {
        _currentPart++;

        if (_currentPart > Parts.Length)
        {
            // On Item Complete
            //Silhouette.SetActive(false);
            Rig.gameObject.SetActive(true);
            //gameObject.SetActive(true);
            GameController.OnMatch.RemoveListener(Recognized);
            GameController.Instance.CurrentItem++;
            return;
        }

        OnRecognize.Invoke();
        Parts[_currentPart].SetActive(true);
    }

    public void SetHolderPosition(Transform target)
    {
        foreach (var p in Parts)
            p.transform.position = target.position;

        Parts[0].SetActive(true);
        Silhouette.SetActive(true);
    }
}
