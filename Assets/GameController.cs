﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameController : MonoBehaviour
{
    private int _currentItem;
    public int CurrentItem
    {
        get { return _currentItem; }
        set
        {
            _currentItem = value;

            if (_currentItem < Items.Count)
            {
                ShowCurrentItem();
            }
        }
    }

    public List<Item> Items = new List<Item>();
    public Transform ItemHolder;

    private SpeechStreaming _watson;

    public static UnityEvent OnMatch = new UnityEvent();
    public static UnityEvent OnStartListening = new UnityEvent();
    public static UnityEvent OnStopListening = new UnityEvent();

    public static GameController Instance;

    private void Awake()
    {
        Instance = this;
        _watson = FindObjectOfType<SpeechStreaming>();
        _watson.OnMatch.AddListener(() => OnMatch.Invoke());
        SpeechStreaming.CurrentWords = Items[_currentItem].MatchWords;
    }

    private void Start()
    {
        CurrentItem = 0;
    }

    private void ShowCurrentItem()
    {
        if (_currentItem != 0)
            Items[_currentItem - 1].gameObject.SetActive(false);

        Items[_currentItem].OnRecognize.AddListener(OnItemSnap);
        Items[_currentItem].gameObject.SetActive(true);
        Items[_currentItem].SetHolderPosition(ItemHolder);
    }

    private void OnItemSnap()
    {
        _watson.Active = true;
    }
}